package com.example.numbersconverter;


import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.number1)
    EditText num1;
    @BindView(R.id.number2)
    EditText num2;
    @BindView(R.id.number3)
    EditText num3;
    @BindView(R.id.binaryNum)
    TextView binary;
    @BindView(R.id.octalNum)
    TextView octal;
    @BindView(R.id.hexNum)
    TextView hex;
    @BindView(R.id.button1)
    Button button1;
    @BindView(R.id.button2)
    Button button2;
    @BindView(R.id.button3)
    Button button3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        button1.setEnabled(false);
        button2.setEnabled(false);
        button3.setEnabled(false);
    }

    @OnTextChanged(R.id.number1)
    public void chekNumber1(CharSequence s) {
        if (!(s.length() == 0)) {
            button1.setEnabled(true);
        } else {
            button1.setEnabled(false);
        }
    }

    @OnTextChanged(R.id.number2)
    public void chekNumber2(CharSequence s) {
        if (!(s.length() == 0)) {
            button2.setEnabled(true);
        } else {
            button2.setEnabled(false);
        }
    }

    @OnTextChanged(R.id.number3)
    public void chekNumber3(CharSequence s) {
        if (!(s.length() == 0)) {
            button3.setEnabled(true);
        } else {
            button3.setEnabled(false);
        }
    }

    @OnClick(R.id.button1)
    public void onButton1Click() {
        binary.setText(Integer.toBinaryString(Integer.parseInt(String.valueOf(num1.getText()))));
    }

    @OnClick(R.id.button2)
    public void onButton2Click() {
        octal.setText(Integer.toOctalString(Integer.parseInt(String.valueOf(num2.getText()))));
    }

    @OnClick(R.id.button3)
    public void onButton3Click() {
        hex.setText(Integer.toHexString(Integer.parseInt(String.valueOf(num3.getText()))));
    }

}
